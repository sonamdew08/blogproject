<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/blog/{username}', 'BlogsController@index')->name('blog.show');
Route::get('/blog/{username}/create', 'BlogsController@create')->name('blog.create');
Route::post('/blog', 'BlogsController@store');
Route::get('/blog/{username}/{id}', 'BlogsController@show')->name('blog.show');
Route::get('/blog/{username}/{id}/edit', 'BlogsController@edit')->name('blog.edit');
Route::put('/blog/{username}/{id}/', 'BlogsController@update')->name('blog.update');
Route::get('/blog/{username}/{id}/delete', 'BlogsController@destroy')->name('blog.destroy');


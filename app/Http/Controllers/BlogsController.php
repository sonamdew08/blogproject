<?php

namespace App\Http\Controllers;
use App\User;
use App\Blog;
use Illuminate\Http\Request;

class BlogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($username)
    {
        $users = User::where('username', $username)->firstOrFail();
        
        return view('blogs.index', [
            'users' => $users
        ]);
    }
    public function create($username)
    {
        $users = User::where('username', $username)->firstOrFail();
        return view('blogs.create', [
            'users' => $users
        ]);
    }
    public function store()
    {
        
        $data = request()->validate([
            'title'=>'required',
            'content'=>'required',
        ]);

        auth()->user()->blogs()->create($data);
                
        return redirect('/blog/' . auth()->user()->username);
    }
    public function show($username, $id)
    {
        $users = User::where('username', $username)->firstOrFail();
        $blog = Blog::where('id', $id)->firstOrFail();;
        
        return view('blogs.blog', [
            'blogs' => $blog,
            'users' => $users
        ]);

    }
    public function edit($username, $id)
    {        
        $users = User::where('username', $username)->firstOrFail();
        $blog = Blog::where('id', $id)->firstOrFail();
        // dd($blog->user_id);
        if($users->id == $blog->user_id){
            return view('blogs.edit', [
                'blogs' => $blog, 
                'users' => $users
            ]);
        }
        else {
            $this->authorize('update', $users->blog);
        }

        
    }

    public function update($username, $id)
    {        
        $users = User::where('username', $username)->firstOrFail();
        $blog = Blog::where('id', $id)->firstOrFail();
        $data = request()->validate([
            'title'=>'',
            'content'=>'',
        ]);
        $blog->update($data);

        return redirect('/blog/' . auth()->user()->username);
    }

    public function destroy($username, $id)
    {
        $blog = Blog::find($id);
        $blog->delete();

        return redirect('/blog/' . auth()->user()->username);
    }
}

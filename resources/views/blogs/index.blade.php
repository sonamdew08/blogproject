@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="mb-3">
                <a href="/blog/{{ Auth::user()->username }}/create" class="btn btn-primary">Create blog</a>
            </div>
            
                @foreach($users->blogs as $blog)
                
                <div class="card p-3 mb-3">
                <div><a href="/blog/{{ Auth::user()->username }}/{{ $blog->id }}"><h2>{{ $blog->title }}</h2></a></div>
                <div><p>{{ $blog->content }}</p></div>
                </div>
                
                @endforeach
            
        </div>
    </div>
</div>
@endsection

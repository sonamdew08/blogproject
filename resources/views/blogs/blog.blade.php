@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            
                
                <div class="card p-3 mb-3">
                <div><a href=""><h2>{{ $blogs->title }}</h2></a></div>
                <div><p>{{ $blogs->content }}</p></div>
                <div><a href="/blog/{{ $users->username }}/{{ $blogs->id }}/edit" class="btn btn-primary">Update</a></div>
                <div><a href="/blog/{{ $users->username }}/{{ $blogs->id }}/delete" class="btn btn-primary mt-3">Delete</a></div>
                </div>
                
            
        </div>
    </div>
</div>
@endsection

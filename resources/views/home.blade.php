@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="d-flex">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in! {{ Auth::user()->username }}
                </div>
                <div class="pt-2 pr-2">
                    
                    <a href="/blog/{{ Auth::user()->username }}" class="btn btn-primary">Blogs</a>
                </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
